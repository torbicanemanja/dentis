import datetime

from django.contrib.auth import login
from rest_framework import generics, permissions, status
from rest_framework.authtoken.serializers import AuthTokenSerializer
from rest_framework.response import Response
from rest_framework.decorators import action, permission_classes

from knox.models import AuthToken
from knox.views import LoginView as KnoxLoginView

from .serializers import ( UserSerializer, ProfileSerializer, 
AppointmentTypeSerializer, AppointmentsSerializer, AppointmentSerializer, WriteAppointmentSerializer,
DentistDataSerializer, ProfileDataSerializer, UserDataSerializer)
from .models import Dentist, Profile, User, Appointment, AppointmentType
from .exceptions import EmailUniqueException

class RegisterAPI(generics.GenericAPIView):
    serializer_class = ProfileSerializer

    def post(self, request):
        request.data['user'] = {"username":"sample", "password":"sample"}
        if len(list(User.objects.filter(email = request.data['email'])))>0:
            raise EmailUniqueException()
        serializer = self.get_serializer(data = request.data)
        serializer.is_valid()
        profile = serializer.save()
        user = profile.user
        Profile.save(user)
        return Response({
            "user": UserSerializer(user, context=self.get_serializer_context()).data,
            "token": AuthToken.objects.create(user)[1]
        })

class LoginAPI(KnoxLoginView):
    permission_classes = (permissions.AllowAny,)

    def post(self, request, _format=None):
        serializer = AuthTokenSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        login(request, user)
        return super(LoginAPI, self).post(request, format=None)

class UserAPI(generics.RetrieveAPIView):
    permission_classes = [
        permissions.IsAuthenticated,
    ]
    serializer_class = UserDataSerializer

    def get_object(self):
        return self.request.user

class ProfileDataAPI(generics.ListAPIView):
    permission_classes = [
        permissions.IsAuthenticated,
    ]
    serializer_class = ProfileDataSerializer

    def get_queryset(self):
        if  user_id := self.request.query_params.get('id', False):
            profile = Profile.objects.filter(**{'id':user_id})
            return profile
        return Profile.objects.filter(user=self.request.user)

class DentistDataAPI(generics.ListAPIView):
    permission_classes = [
        permissions.IsAuthenticated,
    ]
    serializer_class = DentistDataSerializer

    def get_queryset(self):
        if  user_id := self.request.query_params.get('id', False):
            dentist = Dentist.objects.filter(**{'id':user_id})
            return dentist
        return Dentist.objects.filter(user=self.request.user)

class AppointmentTypeAPI(generics.ListAPIView):
    permission_classes = [
        permissions.IsAuthenticated,
    ]

    serializer_class = AppointmentTypeSerializer
    
    def get_queryset(self):
        return AppointmentType.objects.all()

class AppointmentAPI(generics.ListAPIView):

    permission_classes = [
        permissions.IsAuthenticatedOrReadOnly,
    ]

    serializer_class = AppointmentsSerializer

    def post(self, request):
        serializer_class = WriteAppointmentSerializer

        data = dict(request.data)

        for item in data:
            if type(data[item]) == list:
                data[item] = data[item][0]

        data['author'] = request.user.pk

        serializer = serializer_class(data = data)
        serializer.is_valid(raise_exceptions=True)

        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)

    def delete(self, *args, **kwargs):
        appointment_id = self.kwargs.get('id','')
        dentist_id = self.kwargs.get('assigned_dentist',{}).get('id','')
        appointment = Appointment.objects.get(**{'id':appointment_id})
        dentist = Dentist.objects.get(**{'assigned_dentist':dentist_id})
        if self.request.user == appointment.user or ( self.request.user == dentist and dentist is not None ):
            appointment.delete()
            return Response(status=200)
        return Response(status=403)

    
    def get_queryset(self):
        if appointmend_id := self.kwargs.get('appointment_id', None):
            
            self.permission_clases = [permissions.IsAuthenticated]
            self.serializer_class = AppointmentSerializer
            appointment = Appointment.objects.filter(**{'id':appointmend_id})
            if self.request.user.id == appointment.first().user.id or self.request.user.id == appointment.first().assigned_dentist.id:
                return appointment
            return Response(status=403)
        dt_1 = datetime.datetime.strptime(self.request.query_params['date_1'], '%d.%m.%yT%H:%M%z')
        dt_2 = datetime.datetime.strptime(self.request.query_params['date_2'], '%d.%m.%yT%H:%M%z')

        appointments = Appointment.objects.filter(
            appointment_range__startswith__gte=dt_1,
            appointment_range__endswith__lte=dt_2,
        ).order_by('id')
        return appointments

    def put(self, request, **kwargs):

        appointment_id = self.kwargs.get('id', None)
        appointment = Appointment.objects.get(**{'id':appointment_id})
        dentist_id = self.kwargs.get('assigned_dentist',{}).get('id','')
        dentist = Dentist.objects.get(**{'assigned_dentist':dentist_id})

        if (self.request.user == appointment.user or self.request.user == dentist):
            data = {}
            data['user'] = request.data.get('user', appointment.user)
            data['appointment_type'] = request.data.get('type', appointment.appointment_type)
            data['assigned_dentist'] = request.data.get('dentist', appointment.assigned_dentist)
            data['prefered_dentist'] = request.data.get('pdentist', appointment.preffered_dentist)
            data['appointment_range'] = request.data.get('termin', appointment.appointment_range)

            serializer_class = WriteAppointmentSerializer
            serializer = serializer_class(appointment, data=data)
            if serializer.is_valid(raise_exception=True):
                serializer.save()
                return Response(serializer.data)

        return Response(status=403)







