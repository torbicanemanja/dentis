from django.urls import path, re_path
from .views import *
from knox import views as knox_views

urlpatterns = [
    re_path('^api/register/?$', RegisterAPI.as_view(), name='register'),
    re_path('^api/login/?$', LoginAPI.as_view(), name='login'),
    re_path('^api/logout/?$', knox_views.LogoutView.as_view(), name='logout'),
    re_path('^api/profile/?$', ProfileDataAPI.as_view(), name='profile'),
    re_path('^api/types/?$', AppointmentTypeAPI.as_view(), name='types'),
    re_path('^api/dentist/?$',DentistDataAPI.as_view(), name='dentist'),
    re_path('^api/authorize/?$', UserAPI.as_view(), name='authorize_login'),
    re_path('^api/appointment/?$', AppointmentAPI.as_view(), name='appointment'),
    path('api/appointment/<int:appointment_id>/', AppointmentAPI.as_view(), name='appointment_id'),
]