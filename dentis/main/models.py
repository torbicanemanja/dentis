from django.db import models
from django.contrib.auth.models import User
from django.contrib.postgres.constraints import ExclusionConstraint
from django.contrib.postgres.fields import ( RangeOperators, DateTimeRangeField )

class Dentist(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, null=True)
    birth_date = models.DateField()
    phone = models.PositiveIntegerField()
    auto_appointments = models.BooleanField(default = False)
    
    def __str__(self):
        return str(self.user)

class AppointmentType(models.Model):
    appointment_type = models.CharField(max_length=30, null = False, unique = True)
    def __str__(self):
        return str(self.appointment_type)

class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, null=True)
    phone_number = models.CharField(max_length=20) # TO DO: Validate; Verify
    insurance_id = models.PositiveIntegerField(unique=True)
    created = models.DateTimeField(auto_now_add=True)
    bio = models.CharField(max_length=600, blank=True, null=True)
    birth_date = models.DateField(null=True, blank=False)

    def __str__(self):
        return str(self.user)

class Appointment(models.Model):
    user = models.ForeignKey(
        Profile,
        on_delete = models.CASCADE,
        )
    appointment_type = models.ForeignKey(
        AppointmentType,
        on_delete= models.SET_NULL,
        null = True,
        )
    preffered_dentist = models.ForeignKey(
        Dentist,
        on_delete = models.SET_NULL,
        related_name = 'preffered',
        null = True,
        )
    assigned_dentist = models.ForeignKey(
        Dentist,
        on_delete = models.SET_NULL,
        related_name = 'assigned',
        null = True,
    )
    appointment_range = DateTimeRangeField(
        null = True
    )
        
    class Meta:
        constraints = [
            ExclusionConstraint(
                name = "exclude_overlapping_dentist",
                expressions = [
                    ("appointment_range", RangeOperators.OVERLAPS),
                    ("assigned_dentist", RangeOperators.EQUAL)
                ],
            ),
            ExclusionConstraint(
                name = "exclude_overlapping_profile",
                expressions = [
                    ("appointment_range", RangeOperators.OVERLAPS),
                    ("user", RangeOperators.EQUAL)
                ],
            ),
        ]

    def __str__(self):
        return str(self.appointment_range) + " " + str(self.assigned_dentist)