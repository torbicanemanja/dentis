from django.contrib.auth.models import User
from rest_framework import serializers
from django.contrib.postgres.fields import DateTimeRangeField
from .models import AppointmentType, Profile, Appointment, Dentist

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = '__all__'

class RegisterSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username', 'email','password','first_name', 'last_name')
        # extra_kwargs = {'password': {'write_only': True}}

    def create(self, validated_data):
        #print(validated_data)
        user = User.objects.create_user(validated_data['username'], validated_data['email'], validated_data['password'])
        return user

class UserDataSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username', 'email','first_name', 'last_name')


class ProfileDataSerializer(serializers.ModelSerializer):
    user = UserSerializer()

    class Meta:
        model = Profile
        fields = ('id')

class DentistDataSerializer(serializers.ModelSerializer):
    user = UserDataSerializer()

    class Meta:
        model = Dentist
        fields = '__all__'

class DentistSerializer(serializers.ModelSerializer):

    user = RegisterSerializer(required=True)
    auto_appointments = serializers.BooleanField()
    phone = serializers.CharField()

    class Meta:
        model = Dentist
        fields = '__all__'

    def create(self, validated_data):
        print(validated_data)
        user = RegisterSerializer().create(validated_data)
        profile = Dentist.objects.create(user=user, phone_number=validated_data['phone_number'], birth_date=validated_data['birth_date'])
        return profile

class ProfileSerializer(serializers.ModelSerializer):
    user = RegisterSerializer(required=True)
    first_name = serializers.CharField()
    last_name = serializers.CharField()
    username = serializers.CharField()
    email = serializers.EmailField(
        required=True,
    )
    password = serializers.CharField()
    insurance_id = serializers.IntegerField()

    class Meta:
        model = Profile
        fields = '__all__'

    def create(self, validated_data):
        print(validated_data)
        user = RegisterSerializer().create(validated_data)
        profile = Profile.objects.create(user=user, phone_number=validated_data['phone_number'], birth_date=validated_data['birth_date'], insurance_id = validated_data['insurance_id'])
        return profile

class AppointmentTypeSerializer(serializers.ModelSerializer):
    appointment_type = serializers.CharField(
        required=True,
    )

    class Meta:
        model = AppointmentType
        fields = '__all__'

class AppointmentsSerializer(serializers.ModelSerializer):

    appointment_range = DateTimeRangeField()
    assigned_dentist = DentistSerializer()
    appointment_type = AppointmentTypeSerializer()

    class Meta:
        model = Appointment
        fields = ['id', 'appointment_range', 'assigned_dentist', 'appointment_type']

class AppointmentSerializer(serializers.ModelSerializer):

    user = ProfileSerializer()
    appointment_type = AppointmentTypeSerializer()
    preffered_dentist = DentistSerializer()
    assigned_dentist = DentistSerializer()
    range = DateTimeRangeField()

    class Meta:
        model = Appointment
        fields = '__all__'

class WriteAppointmentSerializer():
    class Meta:
        model=Appointment
        fields='__all__'

        def to_representation(self, instance):
            data=super().to_representation(instance)

            data['user']=ProfileSerializer(
                Profile.objects.get(pk=data['user'])).data
            data['appointment_type']=ProfileSerializer(
                Profile.objects.get(pk=data['appointment_type'])).data
            data['preffered_dentist']=ProfileSerializer(
                Profile.objects.get(pk=data['preffered_dentist'])).data
            data['assigned_dentist']=ProfileSerializer(
                Profile.objects.get(pk=data['assigned_dentist'])).data
            data['assigned_dentist']=ProfileSerializer(
                Profile.objects.get(pk=data['assigned_dentist'])).data
            return data