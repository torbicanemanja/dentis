from django.contrib import admin
from main.models import Appointment, AppointmentType, Dentist, Profile

class AuthorAdmin(admin.ModelAdmin):
    pass

@admin.register(Dentist, Profile, AppointmentType, Appointment)
class PersonAdmin(admin.ModelAdmin):
    pass

# Register your models here.
