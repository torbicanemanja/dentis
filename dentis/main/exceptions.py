from rest_framework.exceptions import APIException


class EmailUniqueException(APIException):
    status_code = 400
    default_detail = 'Email already in use'
    default_code = 'bad_request'
