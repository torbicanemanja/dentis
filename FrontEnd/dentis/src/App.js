import './App.css';
import Login from './views/login/Login';
import Register from './views/register/Register';
import Doctor from './views/doctor/Doctor';
import Appointment from './views/appointment/Appointment';
import Profile from './views/profile/Profile';
import { Routes, Route, Link } from "react-router-dom";

function App() {
  return (
    <>
      <Routes>
        <Route path="login" exact element={<Login />} />
        <Route path="register" exact element={<Register />} />
        <Route path="doctor" exact element={<Doctor />} />
        <Route path="" exact element={<Appointment />} />
        <Route path="profile" exact element={<Profile />} />
        <Route path="*" exact element={<Login />} />
      </Routes>
    </>
  );
}

export default App;
